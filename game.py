from random import randint


name = input("Hi, what is your name? ")
answer = input("Hello " + name + ". Would you like to play a game? Select yes or no: ")

def game():
    
    guess = 1
    guess_limit = 5
    
    while guess <= guess_limit:
        print("Guess number {0}:".format(guess))
        month_number = randint(1, 12)
        year_number = randint(1924, 2004)

        print("Is your birthday on {} / {} ?".format(month_number, year_number))
        response = input("yes or no? ")

        if response == "yes":
            print("I got it! Game over!")
            exit()
        else:
            print("Okay, I'll guess again!")
            guess += 1


if answer == "yes":
    print("Perfect, Lets get started!")
    game()

else:
    print("You're no fun :(")
    print("GAME OVER")
    exit()

print("I'm tired of trying, I give up!")
exit()













